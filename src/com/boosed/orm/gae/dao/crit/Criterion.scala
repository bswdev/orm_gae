package com.boosed.orm.gae.dao
package crit
import com.googlecode.objectify.Key

/**
 * Case classes for criterion to apply to query.
 */
sealed abstract class Criterion

/** has ancestor */
case class AncestorCriterion[T](ancestor: Key[T]) extends Criterion
/** property = value */
case class EQCriterion(property: String, value: Any) extends Criterion
/** property > value */
case class GTCriterion(property: String, value: Any) extends Criterion
/** property >= value */
case class GTECriterion(property: String, value: Any) extends Criterion
/** property IN items */
//case class InCriterion[T](property: String, items: List[T]) extends Criterion
case class InCriterion[T] (property: String, items: java.util.List[T]) extends Criterion
/** key starts with literal */
case class KeyCriterion(literal: String) extends Criterion
/** property < value */
case class LTCriterion(property: String, value: Any) extends Criterion
/** property <= value */
case class LTECriterion(property: String, value: Any) extends Criterion
/** property != value */
case class NECriterion(property: String, value: Any) extends Criterion
/** property > 0, to avoid 2 checks */
case class NotZeroCriterion(property: String) extends Criterion
/** property starts with value */
case class StartsWithCriterion(property: String, value: String) extends Criterion

/** start cursor */
case class CursorCriterion(cursor: String) extends Criterion
/** starting offset */
case class OffsetCriterion(start: Int) extends Criterion
/** max return */
case class LimitCriterion(limit: Int) extends Criterion
/** order by */
case class SortCriterion(order: String) extends Criterion