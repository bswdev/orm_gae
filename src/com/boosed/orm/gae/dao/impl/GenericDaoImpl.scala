package com.boosed.orm.gae.dao
package impl
import com.googlecode.objectify.Key
import com.boosed.orm.gae.dao.GenericDao
import com.boosed.orm.gae.dao.crit._
import com.googlecode.objectify.ObjectifyService
import com.googlecode.objectify.ObjectifyService.{ ofy => o }
import com.google.appengine.api.datastore.Cursor
import java.util.Map
import scala.collection.JavaConversions._
import java.util.logging.Logger
import com.boosed.orm.gae.traits.Loggable
import com.googlecode.objectify.cmd.Query
import com.googlecode.objectify.cmd.LoadType
import com.googlecode.objectify.VoidWork
import com.googlecode.objectify.Work
import java.util.HashMap

/**
 * Objectify-based implementation of dao.
 */
class GenericDaoImpl[E](
  val clazz: Class[E])
  //opts: ObjectifyOpts = new ObjectifyOpts)
  extends DAOBaseBridge[E]
  with GenericDao[E]
  with Loggable {

  type ResultsCursor[A] = List[A] Tuple2 String
  
  // register class
  ObjectifyService.register(clazz)

  override def delete(es: E*) = unidelete(es)
  override def delete(id: Long) = o.delete.`type`(clazz).id(id)
  override def delete(id: String) = o.delete.`type`(clazz).id(id)

  override def find(id: String): Option[E] = Option(o.load.`type`(clazz).id(id).now)
  override def find(id: Long): Option[E] = Option(o.load.`type`(clazz).id(id).now)
  override def find(key: Key[E]): Option[E] = Option(o.load.key(key).now)
  //override def find(key: Key[E], keys: Key[E]*): Map[Key[E], E] = ofy.get(key :: keys.toList)
  override def find(keys: Key[E]*): Map[Key[E], E] = o.load.keys(keys)

  // filter is option filter that cannot be performed through GQL (e.g., distance)
  override def find(criteria: Criterion*)(loadGroups: Class[_]*)(implicit filter: E => Boolean): ResultsCursor[E] = {
    val it = createQuery(criteria: _*)(loadGroups: _*).iterator
    // should we use "withFilter" here? not necessarily, withFilter aliases filter for iterators
    it.filter(filter).toList -> Option(it.getCursor).toRight(null).fold(n => n, _.toWebSafeString)
  }

  override def findAll: java.util.List[E] = o.load.`type`(clazz).list

  override def findKeys(criteria: Criterion*)(implicit filter: Key[E] => Boolean): ResultsCursor[Key[E]] = {
    val it = createQuery(criteria: _*)().keys.iterator
    it.filter(filter).toList -> Option(it.getCursor).toRight(null).fold(n => n, _.toWebSafeString)
  }

  // pair of parent map w/ cursor
  override def findParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String = {
    //createQuery(criteria: _*).fetchParents[P] ->
    implicit def passthru = (_: E) => true
    val keys = createQuery(criteria: _*)().keys.map(_.getParent.asInstanceOf[Key[P]])
    o.load.keys(keys.toSeq) -> find(criteria: _*)()._2
    //createQuery(criteria: _*).fetchParents[P] -> find(criteria: _*)._2
  }

  override def persist(es: E*): Map[Key[E], E] = uniput(es)

  implicit def func2Work[R](f: GenericDao[E] => R) = new Work[R] {
    override def run = f(GenericDaoImpl.this)
  }
  
  override def transact[R](task: GenericDao[E] => R) = o.transact(task)
  
  //override def transact(task: GenericDao[E] => Unit) = o.transact(
  //  new Work[Unit] {
  //    override def run = task(GenericDaoImpl.this)
  //  })

  //  override def transact(task: (GenericDao[E]) => Unit) {
  //    while (true)
  //      try {
  //        // create new dao to perform task within
  //        new GenericDaoImpl[E](clazz, new ObjectifyOpts().setBeginTransaction(true)) {
  //          // executes the task in the transactional context of this DAO/ofy
  //          def doTransaction(task: (GenericDao[E]) => Unit) = try {
  //            task(this)
  //            this.ofy.getTxn.commit
  //          } finally
  //            if (this.ofy.getTxn.isActive) this.ofy.getTxn.rollback
  //        }.doTransaction(task)
  //
  //        // if no error, just return (break out of while loop)
  //        return
  //      } catch {
  //        case e => warning("could not perform transaction: " + e.getClass.getName)
  //      }
  //  }

  // must include ordering when using an inequality filter (use ascription to cast LoadType as Query)
  private def createQuery(criteria: Criterion*)(loadGroups: Class[_]*): Query[E] =
    ((o.load.group(loadGroups: _*).`type`(clazz): Query[E]) /: criteria)((q, c) => c match {
      case AncestorCriterion(a) => Option(a).toRight(()).fold(_ => q, q ancestor _)
      case CursorCriterion(c) => Option(c).toRight(()).fold(_ => q, q startAt Cursor.fromWebSafeString(_))
      case EQCriterion(p, v) => Option(p).toRight(()).fold(_ => q, q.filter(_, v))
      case GTCriterion(p, v) => q.order(p).filter(p concat " >", v)
      case GTECriterion(p, v) => q.order(p).filter(p concat " >=", v)
      case InCriterion(p, vs) if !vs.isEmpty => q.filter(p concat " in", vs)
      //case InCriterionJ(p, vs) if !vs.isEmpty => q.filter(p concat " in", vs)
      //case KeyCriterion(l) => q.order("__key__").filter("__key__ >=", new Key(clazz, l)).filter("__key__ <=", new Key(clazz, l concat "\ufffd"))
      case KeyCriterion(l) => q.order("__key__").filterKey(">=", l).filterKey("<=", l concat "\ufffd")
      case LTCriterion(p, v) => q.order(p).filter(p concat " <", v)
      case LTECriterion(p, v) => q.order(p).filter(p concat " <=", v)
      case LimitCriterion(l) => q limit l
      case NECriterion(p, v) => Option(p).toRight(()).fold(_ => q, s => q.filter(s concat " !=", v))
      case NotZeroCriterion(p) => q.order(p).filter(p concat " >", 0)
      case OffsetCriterion(o) => q offset o
      case SortCriterion(s) if (s != "") => q order s
      case StartsWithCriterion(p, v) => q.order(p).filter(p concat " >=", v).filter(p concat " <=", v concat "\ufffd")
      case _ => q
    })
}