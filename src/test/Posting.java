package test;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class Posting implements Serializable {

	@Id
	private String _id;
	
	private long _date = System.currentTimeMillis();

	private int _count = 1;

	private String _referer = "";

	public List<String> geocells;
	
	public Posting() {
		// default no-arg constructor
	}
	
	public String getId() {
		return _id;
	}

	public void setId(String id) {
		_id = id;
	}

	public long getDate() {
		return _date;
	}
	
	public String getReferer() {
		return _referer;
	}

	public void setReferer(String referer) {
		_referer = referer;
	}

	public int getCount() {
		return _count;
	}

	public void setCount(int count) {
		_count = count;

		_date = System.currentTimeMillis();
	}
}