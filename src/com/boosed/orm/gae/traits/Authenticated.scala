package com.boosed.orm.gae
package traits

import scala.collection.JavaConversions._

import com.boosed.orm.gae.dao.AccountDao
import com.boosed.orm.gae.db.Account
import com.google.api.server.spi.response.UnauthorizedException
import com.google.appengine.api.users.User
import com.google.appengine.api.users.UserService

import javax.inject.Inject

// trait for resources requiring authentication
// need to specify all checked exceptions so that container can convert them to responses
trait Authenticated {

  implicit def user2Account(user: User) = acctDao.find(user.getUserId) match {
    // return account
    case Some(x) => x

    // create a new account
    case None =>
      val acct = new Account
      acct.id = user.getUserId
      acct.name = user.getNickname
      acct.email = user.getEmail
      acctDao.persist(acct).head._2
  }

  // checks if there is a logged in user; if true, proceed with f(user), if false throw exception
  @throws[UnauthorizedException]
  private def getCurrentUser[A](f: User => A) = Option(userServ.getCurrentUser)
    .toRight(throw new UnauthorizedException("user not logged in")).fold(identity, f(_))

  //private def getCurrentUser[A](f: User => A) = f(new User("test", "test", System.currentTimeMillis.toString))

  // check if the user is an admin
  @throws[UnauthorizedException]
  def ifAdmin[A](f: Account => A) = getCurrentUser(u =>
    if (userServ.isUserAdmin) f(u) else throw new UnauthorizedException("requires admin access"))

  // utilizes a view bound through implicit transform [User <% Account]
  @throws[UnauthorizedException]
  def ifAuthenticated[A](f: Account => A) = getCurrentUser(f(_))

  // check if the user is authorized
  @throws[UnauthorizedException]
  def ifAuthorized[A](f: Account => A) = getCurrentUser(u =>
    if (u.authorized) f(u) else throw new UnauthorizedException("unauthorized for activity"))

  @Inject var acctDao: AccountDao = _
  @Inject var userServ: UserService = _
}