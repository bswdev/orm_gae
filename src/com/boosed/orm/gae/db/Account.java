package com.boosed.orm.gae.db;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfDefault;

/**
 * User account
 */
@Entity
//@XmlRootElement
public class Account implements Serializable {

	@Id
	public String id;

	// display name
	@Index
	@IgnoreSave(IfDefault.class)
	public String name = "";

	// email (for user service)
	@IgnoreSave(IfDefault.class)
	public String email = "";

	@IgnoreSave(IfDefault.class)
	private transient String gmail = "";

	// creation date
	public Long created;

	/** check whether the account privileges revoked */
	@IgnoreSave(IfDefault.class)
	public Boolean authorized = true;
	
	public Account() {
		// default no-arg constructor
		created = System.currentTimeMillis();
	}

	public Account(String id, String name, String email) {
		this();
		this.id = id;
		this.name = name;
		this.email = email;

		// the user's original email
		gmail = email;
	}
}