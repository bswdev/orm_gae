package com.boosed.orm.gae.db.embed;

import java.io.Serializable;

import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Image with url and blob key.
 */
//@Embed
public class Image implements Serializable {

	@IgnoreSave(IfDefault.class)
	public transient String key = "";

	@IgnoreSave(IfDefault.class)
	public String url = "";

	public Image() {
		// default no-arg constructor
	}

	public Image(String key, String url) {
		super();
		this.key = key;
		this.url = url;
	}
}