package test
import com.boosed.orm.gae.dao.GenericDao
import com.boosed.orm.gae.dao.Locatable

trait PostingDao extends GenericDao[Posting] with Locatable {

}