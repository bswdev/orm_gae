package com.boosed.orm.gae.service
package impl
import com.boosed.orm.gae.service.GenericService
import com.googlecode.objectify.Key
import com.boosed.orm.gae.dao.GenericDao
import com.boosed.orm.gae.dao.crit.Criterion
import java.util.Map
import java.util.logging.Logger
import com.boosed.orm.gae.traits.Loggable

/**
 * Using an existential type instead of type parameter.
 */
abstract class GenericServiceImpl[E](val dao: GenericDao[E])
  extends GenericService[E]
  with Loggable {

  //override type E = dao.E
  implicit def passthru = (_: AnyRef) => true

  override def drop(es: E*): Unit = dao.delete(es: _*)
  override def drop(id: Long): Unit = dao.delete(id)
  override def drop(id: String): Unit = dao.delete(id)

  override def get(id: Long): Option[E] = dao.find(id)
  override def get(id: String): Option[E] = dao.find(id)
  override def get(key: Key[E]): Option[E] = dao.find(key)
  override def get(keys: Key[E]*): Map[Key[E], E] = dao.find(keys: _*)
  //override def get(key: Key[E], keys: Key[E]*): Map[Key[E], E] = dao.find(key, keys: _*)

  override def get(criteria: Criterion*)(loadGroups: Class[_]*)(implicit filter: E => Boolean): List[E] Tuple2 String =
    dao.find(criteria: _*)(loadGroups: _*)(filter)

  override def getAll: java.util.List[E] = dao.findAll

  override def getKeys(criteria: Criterion*)(implicit filter: Key[E] => Boolean): List[Key[E]] Tuple2 String =
    dao.findKeys(criteria: _*)(filter)

  override def getParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String = dao.findParents(criteria: _*)

  override def save(es: E*): Map[Key[E], E] = dao.persist(es: _*)

  override def transact(task: (GenericDao[E]) => Unit) = dao.transact(task)
}