package test
import com.google.inject.Guice
import conf.ServerModule
import com.boosed.orm.gae.dao.crit._
import com.beoui.geocell.GeocellUtils
import com.beoui.geocell.GeocellManager
import com.beoui.geocell.model.Point
import com.beoui.geocell.model.BoundingBox
import com.beoui.geocell.model.CostFunction
import com.boosed.orm.gae.dao.Locatable

object Test {
  def main(args: Array[String]) {

    implicit val passthru = (_: AnyRef) => true

    val injector = Guice.createInjector(new ServerModule)
    val ps = injector.getInstance(classOf[PostingService])
    //println(ps getByCriteria())
    ps.get("hi")

    val p = injector.getInstance(classOf[PostingDao])
    //println(p.findKeys(GTCriterion("name", "al")))

    // easton, pa
    val easton = 40.683197 -> -75.26486

    // moorpark, ca
    val moorpark = 34.301346 -> -118.9011

    // durham, nc
    val durham = 36.028687 -> -78.92398

    // beaufort, sc
    val beaufort = 32.430395 -> -80.644723

    val location = beaufort
    val distance = 300

    val latd = distance / 68.708
    val lond = distance / (69.171 * Math.cos(Math.toRadians(Math.abs(location._1))))

//    // InCriterion("geocells", Locatable.bestBboxSearchCells(new BoundingBox()))
//    val cost = new CostFunction {
//      def defaultCostFunction(cells: Int, resolution: Int): Double = {
//        if (cells > 16) Double.MaxValue else 0
//      }
//    }
//
//    implicit def defaultCostFunction(cells: Int, resolution: Int) =
//      if (cells > 16) Double.MaxValue else 0
//
//    var      time = System.currentTimeMillis()
//    val cellz = ps.locate(location, distance)
//    //println(cellz)
//    println(System.currentTimeMillis() - time)
//    
//    time = System.currentTimeMillis()
//    val cells = GeocellManager.bestBboxSearchCells(new BoundingBox(location._1 + latd, location._2 + lond, location._1 - latd, location._2 - lond), cost)
//    println(cells)
//    println(System.currentTimeMillis() - time)
    ps.locate(location, distance)


    //println(ps.locate(durham, 500))
    //println(ps.distance(beaufort, durham))

    //var s: PostingService = null
    //s.getByCriteria(EQCriterion("name", "none"))
  }
}