package com.boosed.orm.gae
package dao

import com.boosed.orm.gae.db.Account

// common daos

// keep track of accounts
trait AccountDao extends GenericDao[Account] { self: impl.AccountDaoImpl => }