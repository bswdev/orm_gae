package com.boosed.orm.gae.dao.impl;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;
import java.util.Map;

import com.googlecode.objectify.Key;

/**
 * Disambiguated version of dao for overload compatibility problems.
 * 
 * @author dsumera
 * 
 * @param <T>
 */
public class DAOBaseBridge<T> {//extends DAOBase {

	public DAOBaseBridge() {
		super();
	}

	//public DAOBaseBridge(boolean transactional) {
	//	super(transactional);
	//}

	//public DAOBaseBridge(ObjectifyOpts opts) {
	//	super(opts);
	//}

	/**
	 * Universal put signature. Workaround for overloaded Java methods
	 * incompatible with Scala.
	 * 
	 * @param t
	 */
	public Map<Key<T>, T> uniput(List<T> t) {
		// put and return first value
		return ofy().save().entities(t).now();
	}
	
	/**
	 * Universal delete signature workaround.
	 * @param <E> either T or Key<T>
	 * @param e
	 */
	public <E> void unidelete(List<E> e) {
		// deletes keys or objects
		ofy().delete().entities(e).now();
	}
}