package com.boosed.orm.gae
package dao
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import com.beoui.geocell.GeocellManager
import scala.collection.JavaConversions._
import com.boosed.orm.gae.dao.crit._

class BoundingBox(_north: Double, _east: Double, _south: Double, _west: Double) {

  // should we swap N and S?
  val invert = _south > _north

  val _northEast = if (invert) _south -> _east else _north -> _east
  var _southWest = if (invert) _north -> _west else _south -> _west

  // Don't swap east and west to allow disambiguation of
  // antimeridian crossing.

  def northEast = _northEast
  def southWest = _southWest
  def north = _northEast._1
  def south = _southWest._1
  def west = _southWest._2
  def east = _northEast._2
}

object Locatable {

  //3958.75587
  val RADIUS = 3963.18934768692
  val MIN_LONGITUDE = -180.0f
  val MAX_LONGITUDE = 180.0f
  val MIN_LATITUDE = -90.0f
  val MAX_LATITUDE = 90.0f
  val GEOCELL_ALPHABET = "0123456789abcdef"
  val GEOCELL_GRID_SIZE = 4

  // maximum *practical* geocell resolution.
  val MAX_GEOCELL_RESOLUTION = 13;

  // maximum number of geocells to consider for a bounding box search.
  val MAX_FEASIBLE_BBOX_SEARCH_CELLS = 300

  // Direction enumerations.
  val NORTHWEST = -1 -> 1
  val NORTH = 0 -> 1
  val NORTHEAST = 1 -> 1
  val EAST = 1 -> 0
  val SOUTHEAST = 1 -> -1
  val SOUTH = 0 -> -1
  val SOUTHWEST = -1 -> -1
  val WEST = -1 -> 0

  def bestBboxSearchCells(bb: BoundingBox)(implicit costFunction: (Int, Int) => Double): List[String] = {
    if (bb.east < bb.west) {
      val bboxAntimeridian1 = new BoundingBox(bb.north, bb.east, bb.south, Locatable.MIN_LONGITUDE)
      val bboxAntimeridian2 = new BoundingBox(bb.north, Locatable.MAX_LONGITUDE, bb.south, bb.west)
      val antimeridianList = bestBboxSearchCells(bboxAntimeridian1)(costFunction)
      //antimeridianList.addAll(bestBboxSearchCells(bboxAntimeridian2, costFunction))
      return antimeridianList ::: bestBboxSearchCells(bboxAntimeridian2)(costFunction)
    }

    val cellNE = Locatable.compute(bb.northEast, Locatable.MAX_GEOCELL_RESOLUTION)
    val cellSW = Locatable.compute(bb.southWest, Locatable.MAX_GEOCELL_RESOLUTION)

    // The current lowest BBOX-search cost found; start with practical infinity.
    //var minCost = Double.MaxValue

    // The set of cells having the lowest calculated BBOX-search cost.
    //List<String> minCostCellSet = new ArrayList<String>();
    //var minCostCellSet: List[String] = Nil

    // First find the common prefix, if there is one.. this will be the base
    // resolution.. i.e. we don't have to look at any higher resolution cells.
    var minResolution = 0
    val maxResolution = Math.min(cellNE.length(), cellSW.length())
    while (minResolution < maxResolution && cellNE.slice(0, minResolution + 1).startsWith(cellSW.slice(0, minResolution + 1)))
      minResolution += 1

    // Iteravely calculate all possible sets of cells that wholly contain
    // the requested bounding box.

    def findCostCellSet(minResolution: Int): List[String] = {
      @tailrec
      def doit(costSet: List[String], cost: Double, resolution: Int): List[String] = {
        if (resolution > Locatable.MAX_GEOCELL_RESOLUTION)
          costSet
        else {
          val curNE = cellNE.slice(0, resolution)
          val curSW = cellSW.slice(0, resolution)
          //val numCells = Locatable.interpolationCount(curNE, curSW)
          if (Locatable.interpolationCount(curNE, curSW) > Locatable.MAX_FEASIBLE_BBOX_SEARCH_CELLS)
            doit(costSet, cost, resolution + 1)
          else {
          //if (numCells <= Locatable.MAX_FEASIBLE_BBOX_SEARCH_CELLS) {
            val newCostSet = Locatable.interpolate(curNE, curSW) //.sorted
            val newCost = costFunction(newCostSet.length, resolution)
            //if (costFunction == null) {
            //DEFAULT_COST_FUNCTION.defaultCostFunction(cellSet.size(), curResolution);
            //  if (numCells > Math.pow(Locatable.GEOCELL_GRID_SIZE, 2)) Double.MaxValue else 0
            //} else

            if (newCost <= cost) {
              //minCost = cost
              //minCostCellSet = cellSet
              doit(newCostSet, newCost, resolution + 1)
            } else {
              //if(minCostCellSet.length == 0) {
              //    minCostCellSet = cellSet
              //}
              // Once the cost starts rising, we won't be able to do better, so abort.
              //break;
              //return minCostCellSet
              if (costSet.length == 0)
                newCostSet.sorted
              else
                costSet.sorted
            }
          } //else doit(costSet, cost, resolution + 1)
        }
      }

      doit(Nil, Double.MaxValue, minResolution)
    }

    findCostCellSet(minResolution)

    //    for (resolution <- minResolution until (Locatable.MAX_GEOCELL_RESOLUTION + 1)) {
    //      val curNE = cellNE.slice(0, resolution)
    //      val curSW = cellSW.slice(0, resolution)
    //      val numCells = Locatable.interpolationCount(curNE, curSW)
    //
    //      if (numCells > Locatable.MAX_FEASIBLE_BBOX_SEARCH_CELLS) {
    //        continue;
    //      }
    //
    //      List < String > cellSet = GeocellUtils.interpolate(curNE, curSW);
    //      Collections.sort(cellSet);
    //
    //      val cost =
    //        if (costFunction == null) {
    //          DEFAULT_COST_FUNCTION.defaultCostFunction(cellSet.size(), curResolution);
    //        } else {
    //          costFunction.defaultCostFunction(cellSet.size(), curResolution);
    //        }
    //
    //      if (cost <= minCost) {
    //        minCost = cost;
    //        minCostCellSet = cellSet;
    //      } else {
    //        if (minCostCellSet.size() == 0) {
    //          minCostCellSet = cellSet;
    //        }
    //        // Once the cost starts rising, we won't be able to do better, so abort.
    //        break;
    //      }
    //    }

    //        
    //        
    //        for(int curResolution = minResolution; curResolution < GeocellManager.MAX_GEOCELL_RESOLUTION + 1; curResolution++) {
    //            String curNE = cellNE.substring(0, curResolution);
    //            String curSW = cellSW.substring(0, curResolution);
    //
    //            int numCells = GeocellUtils.interpolationCount(curNE, curSW);
    //            if(numCells > MAX_FEASIBLE_BBOX_SEARCH_CELLS) {
    //                continue;
    //            }
    //
    //            List<String> cellSet = GeocellUtils.interpolate(curNE, curSW);
    //            Collections.sort(cellSet);
    //
    //            double cost;
    //            if(costFunction == null) {
    //                cost = DEFAULT_COST_FUNCTION.defaultCostFunction(cellSet.size(), curResolution);
    //            } else {
    //                cost = costFunction.defaultCostFunction(cellSet.size(), curResolution);
    //            }
    //
    //            if(cost <= minCost) {
    //                minCost = cost;
    //                minCostCellSet = cellSet;
    //            } else {
    //                if(minCostCellSet.size() == 0) {
    //                    minCostCellSet = cellSet;
    //                }
    //                // Once the cost starts rising, we won't be able to do better, so abort.
    //                break;
    //            }
    //        }
    //        //logger.log(Level.INFO, "Calculate cells "+StringUtils.join(minCostCellSet, ", ")+" in box ("+bbox.getSouth()+","+bbox.getWest()+") ("+bbox.getNorth()+","+bbox.getEast()+")");
    //        return minCostCellSet;
  }

  private def subdivChar(position: Tuple2[Int, Int]): Char =
    // NOTE: This only works for grid size 4.
    Locatable.GEOCELL_ALPHABET.charAt(
      (position._2 & 2) << 2 |
        (position._1 & 2) << 1 |
        (position._2 & 1) << 1 |
        (position._1 & 1) << 0)

  private def subdivXY(c: Char): Tuple2[Int, Int] = {
    // NOTE: This only works for grid size 4.
    val charI = GEOCELL_ALPHABET.indexOf(c)
    ((charI & 4) >> 1 | (charI & 1) >> 0) -> ((charI & 8) >> 2 | (charI & 2) >> 1)
  }

  private def adjacent(cell: String, dir: Tuple2[Int, Int]): Option[String] = {
    if (cell == null)
      return None

    var dx = dir._1
    var dy = dir._2
    //val cellAdjArr = cell.toCharArray(); // Split the geocell string characters into a list.
    var c = cell
    var i = c.length - 1

    while (i >= 0 && (dx != 0 || dy != 0)) {
      val l = subdivXY(c(i))
      var x = l._1
      var y = l._2

      // Horizontal adjacency.
      if (dx == -1) { // Asking for left.
        if (x == 0) { // At left of parent cell.
          x = GEOCELL_GRID_SIZE - 1 // Becomes right edge of adjacent parent.
        } else {
          x -= 1 // Adjacent, same parent.
          dx = 0 // Done with x.
        }
      } else if (dx == 1) { // Asking for right.
        if (x == GEOCELL_GRID_SIZE - 1) { // At right of parent cell.
          x = 0 // Becomes left edge of adjacent parent.
        } else {
          x += 1 // Adjacent, same parent.
          dx = 0 // Done with x.
        }
      }

      // Vertical adjacency.
      if (dy == 1) { // Asking for above.
        if (y == GEOCELL_GRID_SIZE - 1) { // At top of parent cell.
          y = 0 // Becomes bottom edge of adjacent parent.
        } else {
          y += 1 // Adjacent, same parent.
          dy = 0 // Done with y.
        }
      } else if (dy == -1) { // Asking for below.
        if (y == 0) { // At bottom of parent cell.
          y = GEOCELL_GRID_SIZE - 1; // Becomes top edge of adjacent parent.
        } else {
          y -= 1 // Adjacent, same parent.
          dy = 0 // Done with y.
        }
      }

      //int l2[] = {x,y};
      //cell(i) = 'a'
      //c = "a"
      c = c.updated(i, subdivChar(x -> y))
      //c = c.slice(0, i) + subdivChar(x -> y) concat c.slice(i + 1, c.length())
      //cellAdjArr[i] = subdivChar(x -> y);
      i -= 1
    }
    // If we're not done with y then it's trying to wrap vertically,
    // which is a failure.
    if (dy == 0)
      Option(c)
    else
      None

    // At this point, horizontal wrapping is done inherently.
    //return new String(cellAdjArr);
    //Option(c)
  }

  private def collinear(cell1: String, cell2: String, columnTest: Boolean): Boolean = {
    for (pos <- 0 until Math.min(cell1.length, cell2.length)) {
      val l1 = subdivXY(cell1.charAt(pos))
      val l2 = subdivXY(cell2.charAt(pos))

      // check row collinearity (i.e., assure y's are always the same)
      if (!columnTest && l1._2 != l2._2)
        return false

      // check column collinearity (i.e., assure x's are always the same)
      if (columnTest && l1._1 != l2._1)
        return false
    }

    true
  }

  // compute a geocell from point and resolution
  private def compute(point: Tuple2[Double, Double], resolution: Int): String = {
    var north = Locatable.MAX_LATITUDE
    var south = Locatable.MIN_LATITUDE
    var east = Locatable.MAX_LONGITUDE
    var west = Locatable.MIN_LONGITUDE

    val cell = new StringBuilder
    while (cell.length < resolution) {
      val subcellLonSpan = (east - west) / Locatable.GEOCELL_GRID_SIZE
      val subcellLatSpan = (north - south) / Locatable.GEOCELL_GRID_SIZE

      val x = Math.min((Locatable.GEOCELL_GRID_SIZE * (point._2 - west) / (east - west)).toInt,
        Locatable.GEOCELL_GRID_SIZE - 1)
      val y = Math.min((Locatable.GEOCELL_GRID_SIZE * (point._1 - south) / (north - south)).toInt,
        Locatable.GEOCELL_GRID_SIZE - 1)

      cell append subdivChar(x -> y)

      south += subcellLatSpan * y
      north = south + subcellLatSpan

      west += subcellLonSpan * x
      east = west + subcellLonSpan
    }

    cell.toString
  }

  private def computeBox(cell: String): Option[BoundingBox] = {
    Option(cell) match {
      case Some(c) =>
        var bb = new BoundingBox(90.0, 180.0, -90.0, -180.0)
        val cell = new StringBuilder(c)
        while (cell.length() > 0) {
          val subcellLonSpan = (bb.east - bb.west) / GEOCELL_GRID_SIZE
          val subcellLatSpan = (bb.north - bb.south) / GEOCELL_GRID_SIZE;

          val pos = subdivXY(cell.charAt(0))

          bb = new BoundingBox(bb.south + subcellLatSpan * (pos._2 + 1),
            bb.west + subcellLonSpan * (pos._1 + 1),
            bb.south + subcellLatSpan * pos._2,
            bb.west + subcellLonSpan * pos._1)

          cell.deleteCharAt(0)
        }

        Option(bb)
      case None => None
    }
  }

  private def interpolate(cellNE: String, cellSW: String): List[String] = {
    // 2D array, will later be flattened
    val cellSet = ListBuffer[ListBuffer[String]]()
    //LinkedList<LinkedList<String>> cellSet = new LinkedList<LinkedList<String>>();
    val cellFirst = ListBuffer[String]()
    cellFirst += cellSW
    cellSet += cellFirst

    // First get adjacent geocells across until Southeast--collinearity with
    // Northeast in vertical direction (0) means we're at Southeast.

    def across(cell: String) {
      @tailrec
      def doit(temp: Option[String]): Unit = temp match {
        case None => // do nothing, end
        case Some(x) =>
          // collect x as side-effect
          cellFirst += x

          // continue collecting
          if (!collinear( /*cellFirst.last*/ x, cellNE, true))
            doit(adjacent( /*cellFirst.last*/ x, EAST))
      }

      doit(adjacent( /*cellFirst.last*/ cell, EAST))
    }

    if (!collinear( /*cellFirst.last*/ cellSW, cellNE, true))
      across(cellSW)

    //across(!collinear(cellFirst.last, cellNE, true))

    //        while(!collinear(cellFirst.last, cellNE, true)) {
    //            val cellTmp = adjacent(cellFirst.last, EAST)
    //            if(cellTmp == null) {
    //                break;
    //            }
    //            cellFirst.add(cellTmp);
    //        }

    def up(row: ListBuffer[String]) {
      @tailrec
      def doit(temp: ListBuffer[Option[String]]): Unit = temp.head match {
        case None => // do nothing, end
        case Some(x) =>
          val tempFlat = temp.flatten
          // collect row as side-effect
          cellSet += tempFlat

          // continue collecting
          if (!tempFlat.last.equalsIgnoreCase(cellNE))
            //doit(tempFlat.collect { case s => adjacent(s, NORTH) })
            doit(tempFlat.map(adjacent(_, NORTH)))
      }

      //          val r = row.collect{
      //            case s: String => adjacent(s, NORTH)
      //          }
      //          
      //          r.head match {
      //            case Some(x) =>
      //              cellSet += r.map(_.get)
      //              doit()
      //          }
      
      //doit(row.collect { case s => adjacent(s, NORTH) })
      doit(row.map(adjacent(_, NORTH)))
    }

    def up2(row: ListBuffer[String]) {
      @tailrec
      def doit(temp: ListBuffer[String]): Unit = if (!temp.isEmpty) {
        cellSet += temp
        
        // continue collecting
        if (!temp.last.equalsIgnoreCase(cellNE))
          doit(temp.map(adjacent(_, NORTH)).flatten)
      }

      //doit(row.collect { case s => adjacent(s, NORTH) })
      doit(row.map(adjacent(_, NORTH)).flatten)
    }
        
    if (!cellFirst.last.equalsIgnoreCase(cellNE))
      up2(cellFirst)
    //up(cellFirst.collect {
    //  case s: String => adjacent(s, NORTH)
    //})

    //        // Then get adjacent geocells upwards.
    //        while(!cellSet.last.last.equalsIgnoreCase(cellNE)) {
    //
    //            LinkedList<String> cellTmpRow = new LinkedList<String>();
    //            for(String g : cellSet.getLast()) {
    //                cellTmpRow.add(adjacent(g, NORTH));
    //            }
    //            if(cellTmpRow.getFirst() == null) {
    //                break;
    //            }
    //            cellSet.add(cellTmpRow);
    //        }

    // Flatten cellSet, since it's currently a 2D array.
    //List<String> result = new ArrayList<String>();
    //for(LinkedList<String> list : cellSet) {
    //   result.addAll(list);
    //}
    //return result;
    cellSet.toList.flatten
    //cellSet.flatten
  }

  def interpolationCount(cellNE: String, cellSW: String): Int = {

    val defbb = new BoundingBox(0, 0, 0, 0)
    val bboxNE = computeBox(cellNE).getOrElse(defbb)
    val bboxSW = computeBox(cellSW).getOrElse(defbb)

    val cellLatSpan = bboxSW.north - bboxSW.south
    val cellLonSpan = bboxSW.east - bboxSW.west

    val numCols = (bboxNE.east - bboxSW.west) / cellLonSpan
    val numRows = (bboxNE.north - bboxSW.south) / cellLatSpan

    val totalCols = numCols * numRows * 1.0

    if (totalCols > Int.MaxValue)
      Int.MaxValue
    else
      totalCols.toInt
  }
}

trait Locatable { self: GenericDao[_] =>

  // field name where geohashes are stored
  val cells: String
  
  private[Locatable] implicit def defaultCostFunction(cells: Int, resolution: Int) =
    if (cells > /*Math.pow(Locatable.GEOCELL_GRID_SIZE, 2)*/ 16) Double.MaxValue else 0

  // distance is in miles
  def within(distance: Double, location: Tuple2[Double, Double]): Criterion = {
    // TODO: can use different distance calculations here (if worldwide)
    val latd = distance / 68.708
    val lond = distance / (69.171 * Math.cos(Math.toRadians(Math.abs(location._1))))
    //InCriterion("geocells", Locatable.bestBboxSearchCells(new BoundingBox(location._1 + latd, location._2 + lond, location._1 - latd, location._2 - lond)))
    import com.beoui.geocell.model.{BoundingBox => Box}
    InCriterion(cells, GeocellManager.bestBboxSearchCells(new Box(location._1 + latd, location._2 + lond, location._1 - latd, location._2 - lond), null))
  }

  def geohash(point: Tuple2[Double, Double], max: Int = Locatable.MAX_GEOCELL_RESOLUTION): List[String] =
    (1 to max).map(Locatable.compute(point, _)).toList

  /**
   * Calculates the great circle distance between two points (law of cosines).
   *
   * @param p1: indicating the first point.
   * @param p2: indicating the second point.
   * @return The 2D great-circle distance between the two given points, in meters.
   */
  def distance(p1: Tuple2[Double, Double], p2: Tuple2[Double, Double]): Double = {
    //    def makeDoubleInRange(d: Double): Double = {
    //      var result = d
    //      if (d > 1) {
    //        result = 1
    //      } else if (d < -1) {
    //        result = -1
    //      }
    //
    //      result
    //    }
    //    val time = System.currentTimeMillis()
    //    val p1lat = Math.toRadians(p1._1)
    //    val p1lon = Math.toRadians(p1._2)
    //    val p2lat = Math.toRadians(p2._1)
    //    val p2lon = Math.toRadians(p2._2)
    //    val rv = Locatable.RADIUS * Math.acos(makeDoubleInRange(Math.sin(p1lat) * Math.sin(p2lat)
    //      + Math.cos(p1lat) * Math.cos(p2lat)
    //      * Math.cos(p2lon - p1lon)))

    //val rv = 
    //double lt1 = p1.getLat(), lt2 = p2.getLat();
    /*2 * 3958.75587*/ 7917.51174 * Math.asin(Math.sqrt(Math.pow(Math.sin(Math.toRadians(p2._1 - p1._1) / 2), 2)
      + Math.cos(Math.toRadians(p1._1)) * Math.cos(Math.toRadians(p2._1))
      * Math.pow(Math.sin(Math.toRadians(p2._2 - p1._2) / 2), 2)))

    //println(System.currentTimeMillis() - time)
    //rv
  }
}