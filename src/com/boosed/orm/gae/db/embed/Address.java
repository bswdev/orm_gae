package com.boosed.orm.gae.db.embed;

import java.io.Serializable;

//import com.googlecode.objectify.annotation.Embed;
import com.googlecode.objectify.annotation.IgnoreSave;
import com.googlecode.objectify.condition.IfDefault;

/**
 * Embeddable for address.
 */
public class Address implements Serializable {

	@IgnoreSave(IfDefault.class)
	public String street1 = "";

	@IgnoreSave(IfDefault.class)
	public String street2 = "";

	@IgnoreSave(IfDefault.class)
	public String city = "";

	@IgnoreSave(IfDefault.class)
	public String statoid = "";

	@IgnoreSave(IfDefault.class)
	public String country = "";

	@IgnoreSave(IfDefault.class)
	public String postal = "";

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (!street1.equals(""))
			sb.append(street1).append(',');
		if (!street2.equals(""))
			sb.append(street2).append(',');
		if (!city.equals(""))
			sb.append(city).append(',');
		if (!statoid.equals(""))
			sb.append(statoid).append(',');
		if (!postal.equals(""))
			sb.append(postal).append(',');
		if (!country.equals(""))
			sb.append(country);
		return sb.toString();
	}
}