package com.boosed.orm.gae
package service
import com.googlecode.objectify.Key
import com.boosed.orm.gae.dao.crit.Criterion
import java.util.Map
import com.boosed.orm.gae.dao.GenericDao

trait GenericService[E] {

  // passthru filter
  //implicit def passthru = (_: E) => true

  def drop(es: E*): Unit
  def drop(id: Long): Unit
  def drop(id: String): Unit

  def get(id: Long): Option[E]
  def get(id: String): Option[E]
  def get(key: Key[E]): Option[E]
  def get(keys: Key[E]*): Map[Key[E], E]
  //def get(key: Key[E], keys: Key[E]*): Map[Key[E], E]
  
  def get(criteria: Criterion*)(loadGroups: Class[_]*)(implicit filter: E => Boolean): List[E] Tuple2 String

  def getAll: java.util.List[E]
  
  def getKeys(criteria: Criterion*)(implicit filter: Key[E] => Boolean): List[Key[E]] Tuple2 String
  //def getParents(criteria: Criterion*)()

  def getParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String
  
  def save(es: E*): Map[Key[E], E]
  
  def transact(task: (GenericDao[E]) => Unit)
}