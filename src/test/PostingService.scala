package test
import scala.collection.immutable.List
import com.boosed.orm.gae.service.GenericService
import scala.collection.Seq
import com.boosed.orm.gae.dao.crit.Criterion

trait PostingService extends GenericService[Posting] {
  
  def locate(location: Pair[Double, Double], distance: Double): List[Posting] Pair String
  
  def distance(origin: (Double, Double), destination: (Double, Double)): Double
}