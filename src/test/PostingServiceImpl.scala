package test
import com.boosed.orm.gae.dao.crit.Criterion
import com.boosed.orm.gae.service.impl.GenericServiceImpl
import com.google.inject.Inject

class PostingServiceImpl(dao: PostingDao) extends GenericServiceImpl(dao) with PostingService {

  // injected constructor
  @Inject
  def this(dao: PostingDao, dummy: Int) {
    this(dao)
  }

  override def locate(location: Pair[Double, Double], distance: Double): List[Posting] Pair String = {
    //println(dao.compute(40.922325 -> -72.63708, 10))
    //get(dao.from(1.0 -> 1, 1))
    //40.922325	 -72.63708
    
    println(dao.within(distance, location))
    //println(dao.to(location))
    null
  }
  
  override def distance(origin: (Double, Double), destination: (Double, Double)): Double = dao.distance(origin, destination)
}