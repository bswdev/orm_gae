package com.boosed.orm.gae
package traits
import javax.servlet.http.HttpServletRequest
import com.google.appengine.api.blobstore.BlobKey
import com.google.appengine.api.files.FileService
import com.google.appengine.api.blobstore.BlobstoreService
import com.boosed.orm.gae.db.embed._
import com.google.appengine.api.images.ImagesService
import com.google.appengine.api.images.ImagesServiceFactory
import java.nio.ByteBuffer
import com.google.appengine.api.images.ImagesService.OutputEncoding
import scala.collection.JavaConversions._
import javax.inject.Inject

// trait for resources which manipulate images
trait Imageable extends Keyable {

  // must be implemented since we can't use structural types to match against Java entities
  //def addImage(t: T, image: Image)(implicit user: Key[Account])

  //    blobServ.getUploads(req).firstOption match {
  //      case None => throw ImageException("no image provided")
  //      case Some(x) => x._2.firstOption match {
  //        case None => throw ImageException("no image provided")
  //        case Some(x) =>
  //          var key = x
  //          try {
  //            val (k, url) = resizeImage(x, width, height)
  //            key = k 
  //            Response
  //              .created(UriBuilder.fromUri(url).build())
  //              .build
  //          } catch {
  //            case _ =>
  //              log.severe("could not resize image")
  //              blobServ.delete(key)
  //              throw ImageException("could not process image")
  //          }
  //      }
  //    }

  // retrieve the blob key associated with an image upload
  def getImage(req: HttpServletRequest): Option[BlobKey] = blobServ.getUploads(req).headOption match {
    // the first element of the first list (if exists)
    case Some(x) => x._2.headOption
    case None => None
  }

  // delete image using image reference
  def dropImage(image: Image): Unit = if (image.key != "")
    // only delete if image is present
    blobServ.delete(image.key)

  // delete image using key
  def dropImage(key: Option[BlobKey]) = key match {
    case None => // do nothing
    case Some(x) => blobServ.delete(x)
  }

  // transform an uploaded image to the specified width x height
  def resizeImage(key: BlobKey, width: Int, height: Int): (BlobKey, String) = {
    // resize image to desired dimensions
    val image = imgServ.applyTransform(
      ImagesServiceFactory.makeResize(width, height, 0.5, 0.5),
      //ImagesServiceFactory.makeResize(width, height, false),
      ImagesServiceFactory.makeImageFromBlob(key),
      OutputEncoding.JPEG)

    // delete the old blob
    blobServ.delete(key)

    // create a new blob file with mime-type "image/jpg"
    val file = fileServ.createNewBlobFile("image/jpg")

    // open a channel to write to it and finalize
    val channel = fileServ.openWriteChannel(file, true)
    channel.write(ByteBuffer.wrap(image.getImageData))
    channel.closeFinally

    // now read from the file using the Blobstore API; also return serving URL
    val rv = fileServ.getBlobKey(file)
    rv -> imgServ.getServingUrl(rv)
  }

  @Inject var blobServ: BlobstoreService = _
  @Inject var fileServ: FileService = _
  @Inject var imgServ: ImagesService = _
}