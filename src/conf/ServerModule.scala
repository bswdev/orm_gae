package conf

import com.google.inject.AbstractModule
import test.PostingDao
import test.PostingDaoImpl
import test.PostingService
import test.PostingServiceImpl

class ServerModule extends AbstractModule {

  override protected def configure() {

    // integer (artificial bind to force injection constructor for service)
    bind(classOf[Int]) toInstance 0

    // daos
    bind(classOf[PostingDao]) to classOf[PostingDaoImpl] asEagerSingleton

    // services
    bind(classOf[PostingService]) to classOf[PostingServiceImpl] asEagerSingleton
  }

  //  @Provides
  //  def provideBlobstoreService: BlobstoreService = Option(ServerModule.blobstoreService) match {
  //    case Some(x) => x
  //    case None =>
  //      ServerModule.blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
  //      ServerModule.blobstoreService
  //  }
}

//object ServerModule {
//
//  /** service for storing blobs */
//  private var blobstoreService: BlobstoreService = null
//}