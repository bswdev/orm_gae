package com.boosed.orm.gae
package traits

import scala.annotation.tailrec
import javax.inject.Inject
import javax.inject.Named

// recursive fetch wrt criteria and limits
trait Fetchable[T] {
  
  type ResultsCursor[T] = List[T] Tuple2 String
  
  // params are beginning offset and
  // function which takes current offset and returns list or results with a cursor
  def fetch(offset: Int)(load: Int => ResultsCursor[T]): Int Tuple2 List[T] = {
    // recursive method to collect until limit
    @tailrec
    def find(os: Int, rv: List[T] = Nil): Int Tuple2 List[T] = {
      // collect up to limit or more
      rv.length match {
        // limit reached
        case len if len >= fetchLimit => os -> rv

        // limit not reached
        case len =>
          // search, cursor value is not used
          val (fetch, _) = load(os)

          // check term conditions
          fetch.size match {
            // no matches, set offset to -1
            case 0 => -1 -> (rv ::: fetch)//Pair(-1, rv ::: fetch)
            
            // limit not reached, find more results; increment offset by fetch limit
            case n if n < fetchLimit => find(os + fetchLimit, rv ::: fetch)
            
            // limit reached, return results; increment offset by fetch limit 
            case n => (os + fetchLimit) -> (rv ::: fetch)//Pair(os + fetchLimit, rv ::: fetch)
          }
      }
    }

    find(offset) // execute fetch
  }

  // fetch limit
  @Inject @Named("fetch_limit") protected var fetchLimit: Int = _
}