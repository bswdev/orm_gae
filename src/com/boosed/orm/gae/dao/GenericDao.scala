package com.boosed.orm.gae
package dao
import com.googlecode.objectify.Key
import com.boosed.orm.gae.dao.crit.Criterion
import java.util.Map

/**
 * Base trait for dao.
 */

trait GenericDao[E] {
  // types
  //type E // entity
  //type ID // id for key

  def delete(es: E*): Unit
  def delete(id: Long): Unit
  def delete(id: String): Unit

  // needs implementation in each concrete since Id is not known generally
  def find(id: Long): Option[E]
  def find(id: String): Option[E]
  def find(key: Key[E]): Option[E]
  //def find(key: Key[E], keys: Key[E]*): Map[Key[E], E]
  def find(keys: Key[E]*): Map[Key[E], E]
  
  def find(criteria: Criterion*)(loadGroups: Class[_]*)(implicit filter: E => Boolean): List[E] Tuple2 String
  def findAll: java.util.List[E]
  def findKeys(criteria: Criterion*)(implicit filter: Key[E] => Boolean): List[Key[E]] Tuple2 String
  def findParents[P](criteria: Criterion*): Map[Key[P], P] Tuple2 String

  def persist(es: E*): Map[Key[E], E]
  
  // repeat task in transaction until success
  def transact[R](task: GenericDao[E] => R)
}