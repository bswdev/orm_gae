package com.boosed.orm.gae.dao
package impl
import com.boosed.orm.gae.db.Account
import com.boosed.orm.gae.dao.AccountDao

class AccountDaoImpl extends GenericDaoImpl(classOf[Account]) with AccountDao