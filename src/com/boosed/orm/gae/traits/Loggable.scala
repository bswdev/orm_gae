package com.boosed.orm.gae
package traits

import java.util.logging.Logger

/**
 * Logging behavior.
 */
trait Loggable {

  private[this] var log: Logger = Logger.getLogger(getClass.getName)
  
  def info(msg: String) = log.info(msg)
  def warning(msg: String) = log.warning(msg)
  def severe(msg: String) = log.severe(msg)
  def throwing(clazz: String, method: String, t: Throwable) = log.throwing(clazz, method, t)
}